//Функции m и f представляют собой типичные функции высшего порядка: m является аналогом функции map, а f - аналогом функции filter.

//Функция map применяет заданную функцию к каждому элементу списка (m применяет функцию, переданную в rsi, к данным из списка, начинающегося с rdi).

//Функция filter проводит фильтрацию списка, создавая новый список только с теми элементами, для которых заданная функция возвращает истину (f использует функцию, переданную через rdx, для принятия решения, стоит ли добавить элемент, адресованный через rdi, в новый список).


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Определение структуры узла списка
typedef struct Node {
    long data;
    struct Node *next;
} Node;

// Печать целого числа 
void print_int(long data) {
    printf("%ld ", data);
    fflush(stdout);
}

// Опеределение чётности числа, true, если число нечетное, иначе false
bool p(long data) {
    return (data & 1) != 0;
}

// Добавления элемента в начало односвязного списка
Node *add_element(long data, Node *next) {
    Node *new_element = (Node *)malloc(sizeof(Node)); // Выделение памяти под узел
    if (!new_element) {
        abort();
    }
    new_element->data = data;
    new_element->next = next;
    return new_element;
}

// Перебора всех элементов списка и выполнения переданной функции для данных каждого узла
void m(Node *list, void (*func)(long)) {
    while (list != NULL) {
        func(list->data); 
        list = list->next;  
    }
}

// Создания нового списка, содержащего элементы, удовлетворяющие условию предиката
Node *f(Node *list, bool (*predicate)(long)) {
    Node *res = NULL;
    Node **last_ptr = &res;
    while (list != NULL) {
        if (predicate(list->data)) {
            *last_ptr = add_element(list->data, NULL);
            last_ptr = &(*last_ptr)->next;
        }
        list = list->next;
    }
    return res;
}

int main() {
    long data[] = {4, 8, 15, 16, 23, 42};
    size_t data_length = sizeof(data) / sizeof(data[0]);

    Node *list = NULL;
    for (int i = (int)data_length - 1; i >= 0; --i) {
        list = add_element(data[i], list);
    }
    // Печать всех элементов списка
    m(list, print_int);
    puts("");

    // Фильтрация списка и печать результата
    Node *filtered = f(list, p);
    m(filtered, print_int);
    puts("");

    // Очистка выделенной памяти в ассемблере этот шаг отсутствует
    while (list != NULL) {
        Node *next = list->next;
        free(list);
        list = next;
    }
    while (filtered != NULL) {
        Node *next = filtered->next;
        free(filtered);
        filtered = next;
    }

    return 0;
}

